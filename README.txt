The Overlay Expand adds an icon to overlays that allows you to expand the
overlay to the full browser view effectively breaking you out of overlay mode.

The module is intended for use by anyone that has access to the overlay.

Setup was written by Brian Gilbert and is maintained by Brian Gilbert
(realityloop) and Stuart Clark (deciphered) of Realityloop Pty Ltd.
- http://www.realityloop.com
- http://twitter.com/realityloop



Required modules
--------------------------------------------------------------------------------

* Drupal core Overlay



TODO / Roadmap
--------------------------------------------------------------------------------

- Add a permission.

